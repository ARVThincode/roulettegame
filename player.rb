# frozen_string_literal: true

require 'require_all'
require_rel 'strategies'
require_relative 'strategies/betting_strategy'
require_relative 'roulette/roulette_bet'
require_relative 'roulette/roulette_game'

class Player
  attr_accessor :info, :game

  def initialize(args)
    @info = { money: 0, wage: 0, losses: 0, wins: 0, consecutive_losses: 0, consecutive_wins: 0,
              round_won: false, strategy_name: nil, cancellation_sequence: [], alternative_bet: nil,
              bet: nil, stop_playing: false, predefined_losses: 0, rounds: 0, bet_name: nil }

    @info.merge!(args)
    @strategy = nil
    @bet = new_bet
  end

  def new_bet
    RouletteBet.new
  end

  def new_roulette_game
    RouletteGame.new(@info[:bet_name], @info[:wage])
  end

  def new_betting_strategy
    @strategy = case @info[:strategy_name]
                when :martingale then Martingale.new(initial_wage: @info[:wage], wage: @info[:wage])
                when :waiting
                  Waiting.new(alternative_bet: @info[:alternative_bet],
                              predefined_losses: @info[:predefined_losses])
                when :one_three_two_six
                  OneThreeTwoSix.new(initial_wage: @info[:wage], wage: @info[:wage])
                when :cancellation then Cancellation.new({})
                else Default.new({})
                end
  end

  def use_strategy
    @strategy.update_strategy_information(
      wins: @info[:wins],
      wage: @info[:wage],
      win: @info[:round_won],
      losses: @info[:losses],
      consecutive_wins: @info[:consecutive_wins],
      consecutive_losses: @info[:consecutive_losses],
      cancellation_sequence: @info[:cancellation_sequence]
    )
    @info&.merge!(@strategy.implement)
  end

  def win?(result)
    if @info[:bet].is_a?(Array)
      if @info[:bet].include?(result) then update_wins
      else update_losses
      end
    elsif @info[:bet] == result then update_wins
    else update_losses
    end
    @info[:round_won]
  end

  def update_wins
    @info[:wins] += 1
    @info[:consecutive_wins] += 1
    @info[:consecutive_losses] = 0
    @info[:round_won] = true
  end

  def update_losses
    @info[:losses] += 1
    @info[:consecutive_wins] = 0
    @info[:consecutive_losses] += 1
    @info[:round_won] = false
  end

  def lose?
    @info[:money] = 0 if @info[:money] <= 0
  end

  def play
    @game = new_roulette_game

    @info[:rounds].times do
      break if @info[:stop_playing]

      break puts 'You lose!' if lose?

      if win?(@game.spin_wheel) then @info[:money] += @game.pay
      else @info[:money] -= @game.collect
      end
      use_strategy
    end
  end
end
