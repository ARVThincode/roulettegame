# frozen_string_literal: true

require_relative '../ui/user_interface'
require_relative '../roulette/roulette_bet'
require_relative '../roulette/roulette_game'
require_relative '../player'
require_relative '../statistic'

player = Player.new({})
bet = RouletteBet.new
ui = UserInterface.new(player, bet, player.game)
ui.simulate
statistics = Statistic.new(player)
statistics.summary
