# frozen_string_literal: true

class RouletteGame
  attr_accessor :table_limit
  def initialize(bet, player_wage)
    @bet = bet
    @player_wage = player_wage
  end

  def spin_wheel
    ((0..36).to_a + ['00']).sample
  end

  def pay
    @player_wage * RouletteBet::BET_RATIOS[@bet]
  end

  def collect
    @player_wage
  end
end
