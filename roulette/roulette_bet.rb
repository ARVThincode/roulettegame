# frozen_string_literal: true

class RouletteBet
  BET_RATIOS = { straight: 35, split: 17, street: 11, corner: 8, line: 5, five_numbers: 6,
                 first_twelve: 2, second_twelve: 2, third_twelve: 2, low: 1, high: 1, even: 1,
                 odd: 1, red: 1, black: 1, left_column: 2, middle_column: 2,
                 right_column: 2 }.freeze

  def straight(number)
    return number if number == '00' || number&.between?(0, 36)

    nil
  end

  def left_column_adjacent?(num, adj)
    if left_column.include?(num)
      return false unless adj == ((num + 1) || (num + 3) || (num - 3))
    end
    true
  end

  def middle_column_adjacent?(num, adj)
    if middle_column.include?(num)
      return false unless adj == ((num - 1) || (num + 1) || (num - 3) || (num + 3))
    end
    true
  end

  def right_column_adjacent?(num, adj)
    if right_column.include?(num)
      return false unless adj == ((num - 1) || (num - 3) || (num + 3))
    end
    true
  end

  def adjacent?(num, adj)
    return false unless left_column_adjacent?(num, adj)
    return false unless middle_column_adjacent?(num, adj)
    return false unless right_column_adjacent?(num, adj)

    true
  end

  def split(num, adj)
    return nil if num == adj
    return nil if adj < 1 || adj > 36
    return nil unless adjacent?(num, adj)

    [num, adj]
  end

  def street(num)
    return nil if num.nil? || num % 3 != 1
    return (num..(num + 2)).to_a if num.between?(1, 34)

    nil
  end

  def corner(num)
    return nil if num.nil? || right_column.include?(num)
    return [num, num + 1, num + 3, num + 4] if num.between?(1, 32)

    nil
  end

  def line(num)
    return nil if num.nil?
    return (num..(num + 5)).to_a if num.between?(1, 32) && left_column.include?(num)

    nil
  end

  def five_numbers
    [0, '00', 1, 2, 3]
  end

  def first_twelve
    (1..12).to_a
  end

  def second_twelve
    (13..24).to_a
  end

  def third_twelve
    (25..36).to_a
  end

  def low
    (1..18).to_a
  end

  def high
    (19..36).to_a
  end

  def even
    (1..36).select.each(&:even?)
  end

  def odd
    (1..36).select.each(&:odd?)
  end

  def red
    [1, 3, 5, 7, 9, 12, 14, 16, 18, 19, 21, 23, 25, 27, 30, 32, 34, 36]
  end

  def black
    [2, 4, 6, 8, 10, 11, 13, 15, 17, 20, 22, 24, 26, 28, 29, 31, 33, 35]
  end

  def left_column
    (1..36).select { |number| (number % 3) == 1 }
  end

  def middle_column
    (1..36).select { |number| (number % 3) == 2 }
  end

  def right_column
    (1..36).select { |number| (number % 3).zero? }
  end
end
