# frozen_string_literal: true

require_relative 'user_interface'

module BetType
  BET_ARRAY = %i[straight split street corner line five_numbers first_twelve second_twelve
                 third_twelve low high even odd red black left_column middle_column right_column
                 no_be].freeze

  def self.straight(player, bet)
    puts 'You selected Straight! Which number do you want?'
    number = gets.chomp.to_i
    player.info[:bet] = bet.straight(number)
    player.info[:bet_name] = :straight
  end

  def self.split(player, bet)
    puts 'You selected Split! Which number do you want?'
    number = gets.chomp.to_i
    puts 'Which adjacent number you want?'
    adjacent = gets.chomp.to_i
    player.info[:bet] = bet.split(number, adjacent)
    player.info[:bet_name] = :split
  end

  def self.street(player, bet)
    puts 'You selected Street! Select the first number of the first row:'
    number = gets.chomp.to_i
    player.info[:bet] = bet.street(number)
    player.info[:bet_name] = :street
  end

  def self.corner(player, bet)
    puts 'You selected Corner! Select the upper left number:'
    number = gets.chomp.to_i
    player.info[:bet] = bet.corner(number)
    player.info[:bet_name] = :corner
  end

  def self.line(player, bet)
    puts 'You selected Line! Select the first number of the row:'
    number = gets.chomp.to_i
    player.info[:bet] = bet.line(number)
    player.info[:bet_name] = :line
  end

  def self.five_numbers(player, bet)
    player.info[:bet] = bet.five_numbers
    player.info[:bet_name] = :five_numbers
  end

  def self.first_twelve(player, bet)
    player.info[:bet] = bet.first_twelve
    player.info[:bet_name] = :first_twelve
  end

  def self.second_twelve(player, bet)
    player.info[:bet] = bet.second_twelve
    player.info[:bet_name] = :second_twelve
  end

  def self.third_twelve(player, bet)
    player.info[:bet] = bet.third_twelve
    player.info[:bet_name] = :third_twelve
  end

  def self.low(player, bet)
    player.info[:bet] = bet.low
    player.info[:bet_name] = :low
  end

  def self.high(player, bet)
    player.info[:bet] = bet.high
    player.info[:bet_name] = :high
  end

  def self.even(player, bet)
    player.info[:bet] = bet.even
    player.info[:bet_name] = :even
  end

  def self.odd(player, bet)
    player.info[:bet] = bet.odd
    player.info[:bet_name] = :odd
  end

  def self.red(player, bet)
    player.info[:bet] = bet.red
    player.info[:bet_name] = :red
  end

  def self.black(player, bet)
    player.info[:bet] = bet.black
    player.info[:bet_name] = :black
  end

  def self.left_column(player, bet)
    player.info[:bet] = bet.left_column
    player.info[:bet_name] = :left_column
  end

  def self.middle_column(player, bet)
    player.info[:bet] = bet.middle_column
    player.info[:bet_name] = :middle_column
  end

  def self.right_column(player, bet)
    player.info[:bet] = bet.right_column
    player.info[:bet_name] = :right_column
  end

  def self.no_bet(player)
    puts 'The bet you selected is not valid :('
    player.info[:bet] = nil
    player.info[:bet_name] = nil
  end
end
