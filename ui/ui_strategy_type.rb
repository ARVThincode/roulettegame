# frozen_string_literal: true

module StrategyType
  STRATEGY_ARRAY = %i[martingale waiting one_three_two_six cancellation no_strategy].freeze

  def self.martingale(player, _user_interface = nil)
    player.info[:strategy_name] = :martingale
  end

  def self.waiting(player, __user_interface = nil)
    player.info[:strategy_name] = :waiting
    player.info[:predefined_losses] = user_interface.obtain_predefined_losses
    player.info[:alternative_bet] = user_interface.obtain_alternative_bet
  end

  def self.one_three_two_six(player, _user_interface = nil)
    player.info[:strategy_name] = :one_three_two_six
  end

  def self.cancellation(player, _user_interface = nil)
    select_cancellation_sequence(player, _user_interface = nil)
    player.info[:strategy_name] = :cancellation
  end

  def self.no_strategy(player, _user_interface = nil)
    player.info[:strategy_name] = :default
  end

  def private_class_method.array_sum(player)
    player.info[:cancellation_sequence].inject(0, &:+)
  end

  def private_class_method.validate_cancellation_sequence(player, _user_interface = nil)
    while array_sum(player) < player.info[:wage]
      number = gets.chomp.to_i
      if (number + array_sum(player)) > player.info[:wage]
        puts 'The sum of the numbers are greater than the wage! Enter a smaller number'
      else
        player.info[:cancellation_sequence] << number
      end
    end
  end

  def private_class_method.reduce_cancellation_sequence(player, _user_interface = nil)
    player.info[:wage] = if player.info[:cancellation_sequence].length > 1
                           player.info[:cancellation_sequence].first +
                             player.info[:cancellation_sequence].last
                         else
                           player.info[:cancellation_sequence].first
                         end
  end

  def private_class_method.select_cancellation_sequence(player, _user_interface = nil)
    puts 'Enter a sequnce of numbers that summed are equal to the wage amount:'
    validate_cancellation_sequence(player, _user_interface = nil)
    puts "Cancellation sequence: #{player.info[:cancellation_sequence]}"
    reduce_cancellation_sequence(player, _user_interface = nil)
  end
end
