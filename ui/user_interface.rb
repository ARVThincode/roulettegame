# frozen_string_literal: true

require_relative '../player'
require_relative '../roulette/roulette_bet'
require_relative 'ui_bet_type'
require_relative 'ui_strategy_type'

class UserInterface
  include BetType
  include StrategyType

  def initialize(player, bet, game)
    @player = player
    @bet = bet
    @game = game
  end

  def print_welcome_message
    puts "\n\n|---||---||---||---||---||---||---||---||---|\n|Welcome to the American Roulette "\
    "simulator!|\n|---||---||---||---||---||---||---||---||---|\n\n"
  end

  def print_bet_menu
    puts "Select the bet number you want:\n1.- straight\n2.- split\n3.- street\n4.- corner\n"\
    "5.- line\n6.- five numbers\n7.- first tweleve\n8.- second tweleve\n9.- third tweleve\n"\
    "10.- low\n11.- high\n12.- even\n13.- odd\n14.- red\n15.- black\n16.- left column\n"\
    "17.- middle column\n18.- right column\n\n"
  end

  def print_lose_message
    puts "\n|---||---||---||---||---||---||---||---||---|\n        You've lost all your money!"\
    "|---||---||---||---||---||---||---||---||---|\n\n"
  end

  def print_strategy_menu
    puts "Now let's select a strategy:\n1.- Martingale\n2.- Waiting\n3.- 1-3-2-6 system\n"\
    "4.- Cancellation\n5.- None"
  end

  def obtain_wage
    puts 'What is your budget?'
    @player.info[:money] = gets.chomp.to_i
    puts "\n\nAwesome! Now, what is your wage?"
    @player.info[:wage] = gets.chomp.to_i
    puts "\n\n\n"
  end

  def select_bet_type(bet_type)
    puts "\n-----------------------------------------------------------------"

    if bet_type.between?(1, 18)
      BetType.send(BET_ARRAY[bet_type - 1], @player, @bet)
    else
      BetType.no_bet(@player)
    end
    puts "Current bet: #{@player.info[:bet]}"
    puts '-----------------------------------------------------------------'
  end

  def select_strategy(strategy)
    puts "\n---------------------------"

    if strategy.between?(1, 4)
      StrategyType.send(STRATEGY_ARRAY[strategy - 1], @player, self)
    else
      StrategyType.no_strategy(@player)
    end
    @player.new_betting_strategy
    puts "Current strategy: #{@player.info[:strategy_name]}"
    puts '---------------------------'
  end

  def obtain_predefined_losses
    puts "\nHow many losses you want to wait before changing the bet?:"
    gets.chomp.to_i
  end

  def obtain_alternative_bet
    puts "\nSelect the alternative bet you\'d like to use from the list:"
    print_bet_menu
    bet_type = gets.chomp.to_i
    select_bet_type(bet_type)
  end

  def ask_round_amount
    puts 'How many rounds you want to play?'
    @player.info[:rounds] = gets.chomp.to_i
  end

  def simulate
    print_welcome_message
    obtain_wage
    ask_round_amount
    print_strategy_menu
    select_strategy(gets.chomp.to_i)
    print_bet_menu
    select_bet_type(gets.chomp.to_i)
    @player.play
  end
end
