# frozen_string_literal: true

class Statistic
  def initialize(player)
    @player = player
  end

  def games_won
    puts '------------------------'
    puts "Games won: #{@player.info[:wins]}"
  end

  def games_lost
    puts '------------------------'
    puts "Games lost: #{@player.info[:losses]}"
  end

  def player_balance
    puts '------------------------'
    puts "Player's balance: #{@player.info[:money]}"
    puts '------------------------'
  end

  def winning_percentage(total)
    (@player.info[:wins] * 100) / total
  end

  def losing_percentage(total)
    (@player.info[:losses] * 100) / total
  end

  def percentages
    total = @player.info[:wins] + @player.info[:losses]
    puts '------------------------'
    puts "Winning probability: #{winning_percentage(total)}%"
    puts '------------------------'
    puts "Losing probability: #{losing_percentage(total)}%"
  end

  def summary
    puts "\n\n"
    puts '        Summary:'
    games_won
    games_lost
    percentages
    player_balance
    puts "\n\n"
  end
end
