# frozen_string_literal: true

require_relative 'betting_strategy'

class Cancellation < BettingStrategy
  def next_bet
    { wage: @info[:wage], stop_playing: @stop_playing }
  end

  def implement
    if @info[:win]
      if @info[:cancellation_sequence].length > 1 then delete_array_edges
      elsif @info[:cancellation_sequence].empty? then @stop_playing = true
      else @info[:wage] = @info[:cancellation_sequence].first
      end
    else @info[:cancellation_sequence] << @info[:wage]
    end
    next_bet
  end

  private

  def handle_array_size
    if @info[:cancellation_sequence].empty?
      @stop_playing = true
    elsif @info[:cancellation_sequence].length == 1
      @info[:wage] = @info[:cancellation_sequence].first
    else
      @info[:wage] = @info[:cancellation_sequence].first + @info[:cancellation_sequence].last
    end
  end

  def delete_array_edges
    @info[:cancellation_sequence].shift
    @info[:cancellation_sequence].pop
    handle_array_size
  end
end
