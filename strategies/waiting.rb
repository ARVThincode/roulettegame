# frozen_string_literal: true

require_relative 'betting_strategy'

class Waiting < BettingStrategy
  def next_bet
    { bet: @info[:alternative_bet] }
  end

  def implement
    if @info[:consecutive_losses] >= @info[:predefined_losses]
      next_bet
    else
      {}
    end
  end
end
