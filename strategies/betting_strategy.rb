# frozen_string_literal: true

class BettingStrategy
  attr_accessor :info

  def initialize(args)
    @info = { wage: 0, wins: 0, win: false,
              losses: 0, initial_wage: 0,
              consecutive_wins: 0, alternative_bet: nil,
              predefined_losses: 0, consecutive_losses: 0,
              cancellation_sequence: [] }
    update_strategy_information(args)
  end

  def update_strategy_information(args)
    @info.merge!(args)
  end

  def next_bet
    {}
  end
end
