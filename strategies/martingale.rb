# frozen_string_literal: true

require_relative 'betting_strategy'

class Martingale < BettingStrategy
  def next_bet
    { wage: @info[:wage] }
  end

  def implement
    @info[:wage] = @info[:initial_wage] if @info[:win]
    @info[:wage] *= 2 unless @info[:win]
    next_bet
  end
end
