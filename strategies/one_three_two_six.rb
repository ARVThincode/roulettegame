# frozen_string_literal: true

require_relative 'betting_strategy'

class OneThreeTwoSix < BettingStrategy
  def next_bet
    { wage: @info[:wage] }
  end

  def implement
    case @info[:consecutive_wins]
    when 1 then @info[:wage] *= 3
    when 2 then @info[:wage] *= 2
    when 3 then @info[:wage] *= 6
    else @info[:wage] = @info[:initial_wage]
    end
    next_bet
  end
end
