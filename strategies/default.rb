# frozen_string_literal: true

require_relative 'betting_strategy'

class Default < BettingStrategy
  def next_bet
    {}
  end

  def implement
    next_bet
  end
end
