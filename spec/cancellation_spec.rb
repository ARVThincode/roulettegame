# frozen_string_literal: true

require_relative '../strategies/cancellation'

RSpec.describe Cancellation do
  let(:strategy) { Cancellation.new({}) }

  describe '#next_bet' do
    subject { strategy.next_bet }

    it { is_expected.to eq(stop_playing: nil, wage: 0) }

    context 'when information has been processed' do
      before do
        strategy.info[:wage] = 1000
        strategy.instance_variable_set(:@stop_playing, true)
      end

      it { is_expected.to eq(stop_playing: true, wage: 1000) }
    end
  end

  describe '#implement' do
    subject { strategy.implement }

    it { is_expected.to eq(stop_playing: nil, wage: 0) }

    context 'when player loses' do
      before do
        strategy.info[:win] = false
        strategy.info[:wage] = 100
      end

      it do
        strategy.implement
        expect(strategy.info[:cancellation_sequence]).to eq([100])
      end
    end

    context 'when player wins' do
      before { strategy.info[:win] = true }

      context 'and the cancellation sequence has just one value' do
        before { strategy.info[:cancellation_sequence] = [1] }

        it do
          strategy.implement
          expect(strategy.info[:wage]).to eq(1)
        end
      end

      context 'and the cancellation sequence is empty' do
        before { strategy.info[:cancellation_sequence] = [] }

        it do
          strategy.implement
          expect(strategy.instance_variable_get(:@stop_playing)).to be true
        end
      end

      context 'and the cancellation sequence has more than one value' do
        before { strategy.info[:cancellation_sequence] = [1, 2, 3, 4] }

        it { is_expected.to eq(stop_playing: nil, wage: 5) }

        context 'but the sequence turns empty' do
          before { strategy.info[:cancellation_sequence] = [1, 2] }

          it { is_expected.to eq(stop_playing: true, wage: 0) }
        end

        context 'but the sequence turns just one value' do
          before { strategy.info[:cancellation_sequence] = [1, 2, 3] }

          it { is_expected.to eq(stop_playing: nil, wage: 2) }
        end
      end
    end
  end
end
