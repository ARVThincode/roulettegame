# frozen_string_literal: true

require 'spec_helper'
require_relative '../ui/user_interface'
require_relative '../player'
require_relative '../strategies/betting_strategy'

RSpec.describe Player do
  let(:player) { Player.new({}) }
  let(:strategy) { BettingStrategy.new({}) }

  describe '#new_bet' do
    subject { player.new_bet }

    it { is_expected.to be_instance_of(RouletteBet) }
  end

  describe '#new_betting_strategy' do
    subject { player.new_betting_strategy }

    context 'No strategy defined' do
      before { player.info[:strategy_name] = nil }

      it { is_expected.to be_instance_of(Default) }
    end

    context 'Martingale strategy defined' do
      before { player.info[:strategy_name] = :martingale }

      it { is_expected.to be_instance_of(Martingale) }
    end

    context 'Waiting strategy defined' do
      before { player.info[:strategy_name] = :waiting }

      it { is_expected.to be_instance_of(Waiting) }
    end

    context 'One-three-two-six strategy defined' do
      before { player.info[:strategy_name] = :one_three_two_six }

      it { is_expected.to be_instance_of(OneThreeTwoSix) }
    end

    context 'Cancellation strategy defined' do
      before { player.info[:strategy_name] = :cancellation }

      it { is_expected.to be_instance_of(Cancellation) }
    end
  end

  describe '#win?' do
    subject { player.win?(result) }

    context 'Player won the round' do
      context 'bet is an array' do
        before { player.info[:bet] = [1, 2, 3, 4, 5] }
        let(:result) { 3 }

        it { is_expected.to be true }
      end

      context 'bet is an integer' do
        before { player.info[:bet] = 10 }
        let(:result) { 10 }

        it { is_expected.to be true }
      end
    end

    context 'Player lost the round' do
      context 'bet is an array' do
        before { player.info[:bet] = [1, 2, 3, 4, 5] }
        let(:result) { 10 }

        it { is_expected.to be false }
      end
    end

    context 'bet is an integer' do
      before { player.info[:bet] = 10 }
      let(:result) { 11 }

      it { is_expected.to be false }
    end
  end
end
