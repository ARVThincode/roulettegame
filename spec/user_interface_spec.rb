# frozen_string_literal: true

require 'spec_helper'
require_relative '../ui/user_interface'
require_relative '../player'

RSpec.describe UserInterface do
  let(:bet) { RouletteBet.new }
  let(:player) { Player.new({}) }
  let(:user_interface) { UserInterface.new(player, bet, player.game) }

  describe '#obtain_wage' do
    it do
      allow_any_instance_of(Kernel).to receive(:gets).and_return("1000\n", "1\n")
      expect { user_interface.obtain_wage }.to output("What is your budget?\n\n\n"\
        "Awesome! Now, what is your wage?\n\n\n\n").to_stdout
    end
  end

  describe '#select_bet_type' do
    it do
      expect { user_interface.select_bet_type(6) }.to output(
        <<~BET_MENU

          -----------------------------------------------------------------
          Current bet: [0, "00", 1, 2, 3]
          -----------------------------------------------------------------
        BET_MENU
      ).to_stdout
    end
  end

  describe '#select_strategy' do
    it do
      expect { user_interface.select_strategy(1) }.to output(
        <<~BET_MENU

          ---------------------------
          You selected Martingale!
          ---------------------------
        BET_MENU
      ).to_stdout
    end
  end
end
