# frozen_string_literal: true

require 'spec_helper'
require_relative '../roulette/roulette_bet'

RSpec.describe RouletteBet do
  let(:bet) { RouletteBet.new }

  describe '#straight' do
    subject { bet.straight(number) }

    context 'when argument is not valid' do
      let(:number) { nil }
      it { is_expected.to be_nil }
    end

    context 'when argument has a valid value' do
      let(:number) { 10 }
      it { is_expected.to eq(10) }

      context 'and it is 00' do
        let(:number) { '00' }
        it { is_expected.to eq('00') }
      end
    end

    context 'when argument has an invalid value' do
      let(:number) { 37 }
      it { is_expected.to be_nil }
    end
  end

  describe '#split' do
    subject { bet.split(number, adjacent) }

    context 'when arguments are not valid' do
      let(:number) { nil }
      let(:adjacent) { nil }

      it { is_expected.to be_nil }
    end

    context 'when numbers are adjacent' do
      let(:number) { 1 }
      let(:adjacent) { 2 }

      it { is_expected.to eq([1, 2]) }
    end

    context 'when numbers are not adjacent' do
      let(:number) { 1 }
      let(:adjacent) { 24 }

      it { is_expected.to be_nil }
    end
  end

  describe '#street' do
    subject { bet.street(number) }

    context 'when argument is not valid' do
      let(:number) { nil }
      it { is_expected.to be_nil }
    end

    context 'when number is not on the first column' do
      let(:number) { 2 }
      it { is_expected.to be_nil }
    end

    context 'when number is on the first column' do
      let(:number) { 1 }
      it { is_expected.to eq([1, 2, 3]) }
    end
  end

  describe '#corner' do
    subject { bet.corner(number) }

    context 'when argument is not valid' do
      let(:number) { nil }
      it { is_expected.to be_nil }
    end

    context 'when number is on the right column' do
      let(:number) { 3 }
      it { is_expected.to be_nil }
    end

    context 'when number is valid' do
      let(:number) { 14 }
      it { is_expected.to eq([14, 15, 17, 18]) }
    end
  end

  describe '#line' do
    subject { bet.line(number) }

    context 'when argument is not valid' do
      let(:number) { nil }
      it { is_expected.to be_nil }
    end

    context 'when number is not on the left column' do
      let(:number) { 23 }
      it { is_expected.to be_nil }
    end

    context 'when number is on the left column' do
      let(:number) { 22 }
      it { is_expected.to eq([22, 23, 24, 25, 26, 27]) }

      context 'but is on the last row' do
        let(:number) { 34 }
        it { is_expected.to be_nil }
      end
    end
  end

  describe '#five_numbers' do
    subject { bet.five_numbers }

    it { is_expected.to eq([0, '00', 1, 2, 3]) }
  end

  describe '#first_twelve' do
    subject { bet.first_twelve }

    it { is_expected.to eq((1..12).to_a) }
  end

  describe '#second_twelve' do
    subject { bet.second_twelve }

    it { is_expected.to eq((13..24).to_a) }
  end

  describe '#third_twelve' do
    subject { bet.third_twelve }

    it { is_expected.to eq((25..36).to_a) }
  end

  describe '#low' do
    subject { bet.low }

    it { is_expected.to eq((1..18).to_a) }
  end

  describe '#high' do
    subject { bet.high }

    it { is_expected.to eq((19..36).to_a) }
  end

  describe '#even' do
    subject { bet.even }
    let(:expected_result) { [2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36] }

    it { is_expected.to eq(expected_result) }
  end

  describe '#odd' do
    subject { bet.odd }
    let(:expected_result) { [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35] }

    it { is_expected.to eq(expected_result) }
  end

  describe '#red' do
    subject { bet.red }
    let(:expected_result) { [1, 3, 5, 7, 9, 12, 14, 16, 18, 19, 21, 23, 25, 27, 30, 32, 34, 36] }

    it { is_expected.to eq(expected_result) }
  end

  describe '#black' do
    subject { bet.black }
    let(:expected_result) { [2, 4, 6, 8, 10, 11, 13, 15, 17, 20, 22, 24, 26, 28, 29, 31, 33, 35] }

    it { is_expected.to eq(expected_result) }
  end

  describe '#left_column' do
    subject { bet.left_column }
    let(:expected_result) { [1, 4, 7, 10, 13, 16, 19, 22, 25, 28, 31, 34] }

    it { is_expected.to eq(expected_result) }
  end

  describe '#middle_column' do
    subject { bet.middle_column }
    let(:expected_result) { [2, 5, 8, 11, 14, 17, 20, 23, 26, 29, 32, 35] }

    it { is_expected.to eq(expected_result) }
  end

  describe '#right_column' do
    subject { bet.right_column }
    let(:expected_result) { [3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36] }

    it { is_expected.to eq(expected_result) }
  end
end
