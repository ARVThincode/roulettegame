# frozen_string_literal: true

require_relative '../strategies/default'

RSpec.describe Default do
  let(:strategy) { Default.new({}) }

  describe '#next_bet' do
    subject { strategy.next_bet }

    it { is_expected.to eq({}) }
  end

  describe '#implement' do
    subject { strategy.implement }

    it { is_expected.to eq({}) }
  end
end
