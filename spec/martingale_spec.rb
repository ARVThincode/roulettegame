# frozen_string_literal: true

require_relative '../strategies/martingale'

RSpec.describe Martingale do
  let(:strategy) { Martingale.new({}) }
  let(:initial_wage) { 1000 }

  describe '#next_bet' do
    subject { strategy.next_bet }

    it { is_expected.to eq(wage: 0) }

    context 'when strategy information has been processed' do
      before { strategy.info[:wage] = initial_wage }

      it { is_expected.to eq(wage: 1000) }
    end
  end

  describe '#implement' do
    subject { strategy.implement }
    before { strategy.info[:initial_wage] = initial_wage }

    context 'when player wins' do
      before { strategy.info[:win] = true }

      it { is_expected.to eq(wage: 1000) }
    end

    context 'when player loses' do
      before do
        strategy.info[:win] = false
        strategy.info[:wage] = 1000
      end

      it { is_expected.to eq(wage: 2000) }
    end
  end
end
