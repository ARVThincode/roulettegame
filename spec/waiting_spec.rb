# frozen_string_literal: true

require_relative '../strategies/waiting'

RSpec.describe Waiting do
  let(:strategy) { Waiting.new({}) }
  before { strategy.info[:alternative_bet] = [1, 2] }

  describe '#next_bet' do
    subject { strategy.next_bet }

    it { is_expected.to eq(bet: [1, 2]) }
  end

  describe '#implement' do
    subject { strategy.implement }
    before { strategy.info[:predefined_losses] = 3 }

    it { is_expected.to eq({}) }

    context 'when the consecutive losses are equal to the predefined losses' do
      before do
        strategy.info[:predefined_losses] = 3
        strategy.info[:consecutive_losses] = 3
      end

      it { is_expected.to eq(bet: [1, 2]) }
    end
  end
end
