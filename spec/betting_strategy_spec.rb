# frozen_string_literal: true

require_relative '../strategies/betting_strategy'

RSpec.describe BettingStrategy do
  let(:strategy) { BettingStrategy.new({}) }

  describe '#update_strategy_information' do
    let(:args) { {} }

    it do
      expect(strategy.info[:win]).to eq(false)
      expect(strategy.info[:wage]).to eq(0)
      expect(strategy.info[:wins]).to eq(0)
      expect(strategy.info[:losses]).to eq(0)
    end

    context 'when information has changed' do
      let(:args) do
        {
          win: true,
          wage: 1000,
          wins: 3,
          losses: 3
        }
      end

      it do
        strategy.update_strategy_information(args)
        expect(strategy.info[:wage]).to eq(1000)
        expect(strategy.info[:win]).to eq(true)
        expect(strategy.info[:wins]).to eq(3)
        expect(strategy.info[:losses]).to eq(3)
      end
    end
  end

  describe '#next_bet' do
    subject { strategy.next_bet }

    it { is_expected.to eq({}) }
  end
end
