# frozen_string_literal: true

require_relative '../strategies/one_three_two_six'

RSpec.describe OneThreeTwoSix do
  let(:strategy) { OneThreeTwoSix.new({}) }
  let(:initial_wage) { 1000 }

  describe '#next_bet' do
    subject { strategy.next_bet }

    it { is_expected.to eq(wage: 0) }

    context 'when strategy information has been processed' do
      before { strategy.info[:wage] = 1000 }

      it { is_expected.to eq(wage: 1000) }
    end
  end

  describe '#implement' do
    subject { strategy.implement }
    before do
      strategy.info[:wage] = 1000
      strategy.info[:initial_wage] = initial_wage
    end

    context 'when consecutive wins is 0' do
      it { is_expected.to eq(wage: 1000) }
    end

    context 'when there has been 1 consecutive wins' do
      before { strategy.info[:consecutive_wins] = 1 }

      it { is_expected.to eq(wage: 3000) }
    end

    context 'when there has been 2 consecutive wins' do
      before { strategy.info[:consecutive_wins] = 2 }

      it { is_expected.to eq(wage: 2000) }
    end
    context 'when there has been 3 consecutive wins' do
      before { strategy.info[:consecutive_wins] = 3 }

      it { is_expected.to eq(wage: 6000) }
    end
  end
end
